#if !defined(__CLING__) || defined(__ROOTCLING__)
// ROOT header
#include <TROOT.h>
#include <TChain.h>
#include <TH2.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
// O2 header
#include "DataFormatsTRD/TriggerRecord.h"
#include "DataFormatsTRD/Digit.h"
#include "DataFormatsTRD/Tracklet64.h"
#include "DataFormatsTRD/Constants.h"
#endif

using namespace o2::trd;
using namespace o2::trd::constants;

void checkTracklets()
{
  TChain chain("o2sim");
  chain.AddFile("trdtracklets.root");
  std::vector<TriggerRecord> trigIn, *trigInPtr{&trigIn};
  std::vector<Tracklet64> tracklets, *trackletsInPtr{&tracklets};
  chain.SetBranchAddress("TrackTrg", &trigInPtr);
  chain.SetBranchAddress("Tracklet", &trackletsInPtr);

  //auto fOut = new TFile("trackletsOutput.root", "recreate");
  auto hDet = new TH1F("det", "Detector number for tracklet;detector;counts", 540, -0.5, 539.5);
  auto hY = new TH1F("ypos", "Uncalibrated tracklet Y position", 100, -60, 60);
  auto hTrigger = new TH1F("trigger", "Number of TRD triggers per TF;# trigger;counts", 200, 0, 200);


  int countEntries = 0;
  int countTrigger = 0;

  for (int iEntry = 0; iEntry < chain.GetEntries(); ++iEntry) {
    chain.GetEntry(iEntry); // for each TimeFrame there is one tree entry
    ++countEntries;
    countTrigger += trigIn.size();
    hTrigger->Fill(trigIn.size());
    for (const auto& tracklet : tracklets) {
      int det = tracklet.getHCID() / 2;
      hDet->Fill(det);
      auto y = tracklet.getUncalibratedY();
      hY->Fill(y);
    }
  }
  auto c1 = new TCanvas("c1", "c1");
  hDet->Draw();
  c1->SaveAs("tracklets-det.png");
  auto c2 = new TCanvas("c2", "c2");
  hY->Draw();
  c2->SaveAs("tracklets-y.png");

  /*
  hDet->Write();
  delete hDet;
  hY->Write();
  delete hY;

  fOut->Close();
  */

  printf("Got in total %i trigger from %i TFs\n", countTrigger, countEntries);

}
