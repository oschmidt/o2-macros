#if !defined(__CLING__) || defined(__ROOTCLING__)
// ROOT header
#include <TFile.h>
// O2 header
#include "DataFormatsTRD/CalVdriftExB.h"
#include "DataFormatsTRD/Constants.h"
#include "CCDB/CcdbApi.h"
#include "CCDB/BasicCCDBManager.h"

#include <map>
#include <string>
#include <vector>
#include <chrono>

#endif


void ccdbDownload()
{
  auto& ccdbmgr = o2::ccdb::BasicCCDBManager::instance();
  auto timeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  ccdbmgr.setTimestamp(timeStamp);

  auto cal = ccdbmgr.get<o2::trd::CalVdriftExB>("TRD/Calib/CalVdriftExB");

  for (int iDet = 0; iDet < 540; ++iDet) {
    printf("Chamber %i: vDrift = %.3f, ExB = %.3f\n", iDet, cal->getVdrift(iDet), cal->getExB(iDet));
  }

  return;
}
